#! /bin/bash

############ script for the installation of Odoo13 on docker container ################
echo "installing packages required by Docker"
sudo yum install -y epel-release yum-utils device-mapper-persistent-data lvm2

echo "Adding the docker repository"
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

echo "installing Docker"
sudo yum install -y docker-ce-19.03.13

echo "Starting and enabling Docker service"
sudo systemctl start docker
sudo systemctl enable docker

echo "Searching for Odoo and Postgresql image"

sudo docker search odoo
echo " ---------------------------------------------------------------------"
sudo docker search postgresql

echo "Pulling official image of Odoo13 and Postgresql12"

sudo docker pull odoo:13
echo"-----------------------------------------------------------------------"
sudo docker pull postgres:12.4

echo "Starting a postgresql12 server with a postgres as a name for the database, odoo as a user and a password for the database"

sudo docker run -d  -e POSTGRES_DB=postgres -e POSTGRES_USER=odoo  -e POSTGRES_PASSWORD=odoo --name db postgres:12.4

echo "Starting an odoo13 container and linked it to postgresql server"
 
sudo docker run -d  -p 8069:8069 --name odoo --link db:db -t odoo:13
