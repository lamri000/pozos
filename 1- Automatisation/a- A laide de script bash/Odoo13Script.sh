#! /bin/bash

############  The installation have to be done with root or sudo-enabled user ############

echo "1.installing epel release  and ius community  and ymu-utils "
sudo yum install -y epel-release && yum install -y https://repo.ius.io/ius-release-el7.rpm && yum -y install  yum-utils

echo "2.updating repisitories "
sudo yum -y update

echo "3.installing important commands "
sudo yum install -y git wget nodejs-less libxslt-devel bzip2-devel openldap-devel libjpeg-devel freetype-devel npm

echo " 4.installing database postgresql12"
echo "A- Downloading postgresql package "
sudo yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
echo " B- installing postgresql12 package "
sudo  yum-config-manager --enable pgdg12
 sudo yum install -y postgresql12 postgresql12-server postgresql12-contrib postgresql12-libs
echo "B. insiallizing and starting database"
sudo /usr/pgsql-12/bin/postgresql-12-setup initdb
sudo systemctl enable --now postgresql-12
sudo systemctl start postgresql-12
sudo firewall-cmd --add-service=postgresql --permanent
sudo firewall-cmd --reload

echo "5. installing wkhtmltopdf"
sudo  yum install -y https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox-0.12.5-1.centos7.x86_64.rpm 

echo "6. Building yum cache "
sudo yum makecache fast

echo "7. installing python3, dependencies and developement tools"
sudo yum -y install python36 python36-devel python36-pillow python36-lxml  libxml2-devel libjpeg-devel libxml2 libxslt  libpng libjpeg openssl icu libX11 libXext libXrender xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi python3-pip python3-setuptools 
sudo echo 'alias python='python3'' >> ~/.bashrc
source  ~/.bashrc
sudo yum groupinstall 'Development Tools' -y

echo "8. Installing Nodejs packages as required by Odoo 13 server"
sudo npm install -g less less-plugin-clean-css -y

echo "9. creating a postgresql user for odoo"
sudo su - postgres -c "createuser -s odoo"

echo  "10. cerating odoo user"
sudo adduser --system --home-dir=/opt/odoo --shell=/bin/bash -m  odoo

echo "11. creating directories as required by Odoo and adjust their ownership"
sudo mkdir /etc/odoo && mkdir sudo /var/log/odoo
sudo chown -R odoo:odoo /opt/odoo && sudo chown -R odoo:odoo /var/log/odoo

echo "12. installing Odoo 13 Server on CentOS 7"
sudo git clone --depth=1 --branch=13.0 https://github.com/odoo/odoo.git /opt/odoo/odoo

echo "13.installing python list lirequirements for odoo user"
sudo pip3.6 install  -r /opt/odoo/odoo/requirements.txt

echo "14. starting odoo and creating config file"
sudo su - odoo -c "/opt/odoo/odoo/odoo-bin --addons-path=/opt/odoo/odoo/addons -s --stop-after-init"

echo "15. moving config file to /etc/odoo directory"
sudo mv /opt/odoo/.odoorc /etc/odoo/odoo.conf

echo "16.setting location of Odoo logfile in config file"
sudo sed -i "s,^\(logfile = \).*,\1"/var/log/odoo/odoo-server.log"," /etc/odoo/odoo.conf

echo "17. creating symbolic link for Odoo executable"
sudo ln -s /opt/odoo/odoo/odoo-bin /usr/bin/odoo

echo "18.creating Odoo service on CentOS 7"
sudo cp /opt/odoo/odoo/debian/odoo.service /usr/lib/systemd/system/
l
echo "19.enabling and start Odoo service"
sudo systemctl enable --now odoo.service
sudo firewall-cmd --permanent --add-port=8069/tcp
sudo firewall-cmd --reload

echo "20. verifying odoo version"
sudo odoo --version
